const containerEl = document.getElementById('container');

const cardsLenght = 16;
const cards = [];

let previosShownCard = undefined;

let icons = [
    'book',
    'search',
    'key',
    'cog',
    'star',
    'phone',
    'music',
    'money',
]

// iconsni 2 marta ishlatish un
icons.push(...icons);

for (let i = 0; i < 100; i++) {
    const idx1 = Math.floor(Math.random() * icons.length);
    const idx2 = Math.floor(Math.random() * icons.length);

    const temp = icons[idx1];
    icons[idx1] = icons[idx2];
    icons[idx2] = temp;
    
}

for (let i = 0; i < cardsLenght; i++) {
    const cardEl = document.createElement('div');
    cardEl.classList.add('card');
    cardEl.innerHTML=`
    <div class="front" > 
        <i class="fa fa-${icons[i]}"> </i>
    </div>
    <div class="back" > 
        <i class="fa fa-trophy"></i> 
    </div>
    `
    
    cardEl.addEventListener('click', ()=> {
        if (!cardEl.classList.contains('show')) {
            cardEl.classList.add('show')
        } 
        // else{
        //     cardEl.classList.remove('show')
        // }

        if (!previosShownCard) {
            previosShownCard = cardEl
        } else{
            const iconOne = previosShownCard.querySelector('i').classList[1];

            const iconTwo = cardEl.querySelector('i').classList[1];

            if (iconOne !== iconTwo) {
                const temp = previosShownCard;
                setTimeout(() => {
                    temp.classList.remove('show')
                    cardEl.classList.remove('show')
                }, 800);
            }
            previosShownCard = undefined

        }
    })
    cards.push(cardEl);

    containerEl.appendChild(cardEl)
}